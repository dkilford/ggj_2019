﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.SceneManagement;

public class UIManager : MonoSingleton<UIManager>
{
    public TMP_Text m_objectiveText = null;

    Animator m_animator = null;
    bool m_levelCompleted = false;

    void Start()
    {
        
    }


    void Update()
    {
        
    }

    public void PlayIntroAnimation(WinCondition condition, float time, float percentage)
    {
        m_animator = GetComponent<Animator>();

        if(condition == WinCondition.TIME)
        {
            m_objectiveText.text = string.Format("Collect all rubbish in {0} seconds!", time.ToString("0"));
        }
        else if(condition == WinCondition.COMPLETION)
        {
            m_objectiveText.text = string.Format("Collect {0}% of the rubbish in the level!", percentage.ToString("0"));
        }

        m_animator.Play("CanvasCollection");
    }

    public void OnIntroComplete()
    {
        GameManager.Instance.OnIntroComplete();
    }

    public void StartNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void PlayLevelComplete()
    {
        if(m_levelCompleted == false)
        {
            m_levelCompleted = true;
            m_animator.Play("LevelCompleteAnim");
        }
    }
}

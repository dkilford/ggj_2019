using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class CompilerOptionsEditorScript
{
	static bool waitingForStop = false;

	static string[] FOLDERS = new string[]
	{
		"Assets/Art", "Materials",
		"Assets/Art", "Models",
		"Assets/Art", "Animations",
		"Assets", "Prefabs",
		"Assets", "Sound"
	};

	static CompilerOptionsEditorScript()
	{
		EditorApplication.update += OnEditorUpdate;

		for(int i=0; i<FOLDERS.Length; i+= 2)
		{
			if(AssetDatabase.IsValidFolder(System.IO.Path.Combine(FOLDERS[i], FOLDERS[i + 1] )) == false)
			{
				AssetDatabase.CreateFolder(FOLDERS[i], FOLDERS[i + 1]);
			}
		}
	}

	static void OnEditorUpdate()
	{
		if (!waitingForStop
			&& EditorApplication.isCompiling
			&& EditorApplication.isPlaying)
		{
			EditorApplication.LockReloadAssemblies();
			EditorApplication.playModeStateChanged
				 += PlaymodeChanged;
			waitingForStop = true;
		}
	}

	static void PlaymodeChanged(PlayModeStateChange state)
	{
		if (EditorApplication.isPlaying)
			return;

		EditorApplication.UnlockReloadAssemblies();
		EditorApplication.playModeStateChanged
			 -= PlaymodeChanged;
		waitingForStop = false;
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class OrbitMovePoint : MovePoint
{
    //public float m_radius = 1.5f;

    public override void SuckUpdateCompletion()
    {
        Vector3 orbitPoint = GetOrbitPoint();
        m_pController.MoveToPoint(orbitPoint, this);
    }    
}

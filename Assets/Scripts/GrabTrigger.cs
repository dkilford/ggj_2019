﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class GrabTrigger : MonoBehaviour
{
    public float m_releaseForce = 100.0f;
    Rigidbody2D m_suckedObject = null;
    PlayerController m_pController = null;

    void Start()
    {
        m_pController = PlayerController.Instance;
    }

    // void FixedUpdate()
    // {
    //     if(m_suckedObject != null && m_pController.GetIsSuckDown() == false && m_pController.GetForceReleaseSuck() == false)
    //     {
    //         Vector3 dir = m_pController.transform.position - transform.position;
    //         dir.Normalize();
    //         m_suckedObject.AddForce(m_releaseForce * -dir);
    //         //m_suckedObject.transform.SetParent(null);
    //         m_suckedObject = null;
    //     }
    // }

    void OnTriggerEnter2D(Collider2D col)
    {
        TriggerEffect(col);
    }

    void OnTriggerStay2D(Collider2D col)
    {
        TriggerEffect(col);
    }

    void TriggerEffect(Collider2D col)
    {
        Binable binable = col.GetComponent<Binable>();
        if(binable != null && binable.m_destroyOnCollect == true)
        {
            binable.Bin();
            m_pController.PlaySuckInAnim();
            CameraController.Instance.ShakeGentle();
            return;
        }

        if(m_suckedObject != null || m_pController.GetIsSucking() == false)
        {
            return;
        }

        if(col.CompareTag("Suckable") == true)
        {
            m_suckedObject = col.GetComponent<Rigidbody2D>();

            if(m_suckedObject != null)
            {
                //m_suckedObject.transform.SetParent(transform.parent);
                m_suckedObject.isKinematic = true;
                m_suckedObject.velocity = Vector2.zero;
                m_suckedObject.angularVelocity = 0;
                m_pController.PlaySuckInAnim();

                Physics2D.IgnoreCollision(m_pController.GetComponent<Collider2D>(), col, true);
                //m_pController.LockPlayer();
                CameraController.Instance.ShakePickup();
            }
        } 
    }

    public Rigidbody2D GetSuckedObject()
    {
        return m_suckedObject;
    }

    public void Clear()
    {
        if(m_suckedObject != null)
        {
            Physics2D.IgnoreCollision(m_pController.GetComponent<Collider2D>(), m_suckedObject.GetComponent<Collider2D>(), false);
        }

        m_suckedObject = null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class SetCameraPosTrigger : MonoBehaviour
{
    public GameObject m_lookTarget;
    public bool m_triggerMultipleTimes = true;

    bool m_triggered = false;

    void Start()
    {
        
    }


    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(m_triggered == true && m_triggerMultipleTimes == false)
        {
            return;
        }

        if(col.CompareTag("Player") == true)
        {
            CameraController.Instance.SetLookPos(m_lookTarget.transform.position);
            m_triggered = true;
        }
    }
}

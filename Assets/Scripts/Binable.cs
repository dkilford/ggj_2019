﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class Binable : MonoBehaviour
{
    public bool m_destroyOnCollect = false;
    public int m_binableValue = 4;

    bool m_destroying = false;

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.relativeVelocity.magnitude > 10.0f)
        {
            CameraController.Instance.ShakeGentle();
        }
    }

    public void Bin()
    {
        if(m_destroying == true)
        {
            return;
        }

        m_destroying = true;
        GameManager.Instance.OnBinned(this);
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class CameraController : MonoSingleton<CameraController>
{
    public float m_smoothing = 0.15f;
    public float m_maxSpeed = 2.0f;
    
    Animator m_animator = null;
    Vector3 m_targetPos;
    Vector3 m_velocity;
    Vector3 m_startPos;

    void Start()
    {
        m_animator = GetComponent<Animator>();
        m_startPos = transform.position;
        m_targetPos = m_startPos;
    }


    void LateUpdate()
    {
        SetPosition(Vector3.SmoothDamp(transform.position, m_targetPos, ref m_velocity, m_smoothing, m_maxSpeed));
    }

    public void ShakeGentle()
    {
        m_animator.Play("CameraShakeGentle");
    }

    public void ShakeStrong()
    {
        m_animator.Play("CameraShakeStrong");
    }

    public void ShakePickup()
    {
        m_animator.Play("CameraShakePickup");
    }

    void SetPosition(Vector3 pos)
    {
        pos.z = m_startPos.z;
        transform.position = pos;
    }

    public void SetLookPos(Vector3 pos)
    {
        m_targetPos = pos;
    }
}

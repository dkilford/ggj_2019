﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoSingleton<PlayerController>
{
	public float m_targetSuckForce = 3.0f;
    public AnimationCurve m_forceFalloffCurve;
    public AnimationCurve m_moveCurve;
    public AnimationCurve m_thrustFalloffCurve;
    public Collider2D m_vacuumCollider = null;
    public SuckTrigger m_suckTrigger = null;
    public float m_suckForce = 1.0f;
    public GrabTrigger m_grabTrigger = null;
    public float m_shootForce = 100.0f;
    public float m_moveTimeScale = 0.2f;
	public float m_suckDistance = 20.0f;
    public float m_thrustDistance = 20.0f;
	public LayerMask m_suckLayer;
    public Animator m_animator = null;
    public float m_suckPassiveForce = 100.0f;
    public float m_thrustPassiveForce = 100.0f;
    public float m_heldMoveTowardsSpeed = 2.0f;
    public float m_heldMoveTowardsOffset = 0.5f;
    public float m_passSpeed = 50.0f;
    public AudioSource m_shootSound = null;
    public AudioSource m_suckStart = null;
    public AudioSource m_suckLoop = null;
    public AudioLowPassFilter m_suckLoopPass = null;
    public ParticleSystem m_dustParticles = null;
    
    bool m_forceReleaseSuck = false;
    bool m_alive = true;
    bool m_wasSucking = false;
    Vector3 m_heldMoveTowardsVelocity = Vector3.zero;
	MovePoint m_currentPoint = null;
    Coroutine m_moveRoutine = null;
    Camera m_camera = null;
    GameObject m_suckedObject = null;
    List<Collider2D> m_stayingObjects = new List<Collider2D>();
    List<Pickupable> m_lastStayingObjects = new List<Pickupable>();
    Rigidbody2D m_rigidBody = null;

    void Start()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
        m_camera = Camera.main;
        Application.targetFrameRate = 60;
        m_animator = GetComponent<Animator>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) == true)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if(GameManager.Instance.GetHasGameStarted() == false)
        {
            return;
        }

        if(m_alive == false)
        {
            m_shootSound.Stop();
            m_suckStart.Stop();
            m_suckLoop.Stop();
            return;
        }

        if(GetIsSuckDown() == false)
        {
            m_forceReleaseSuck = false;
        }

        Vector2 dir = GetOffset(Input.mousePosition);
        dir.Normalize();

        if(m_currentPoint != null && m_currentPoint.m_orbit == true && m_moveRoutine == null && m_currentPoint.GetIsPlayerStuck() == true)
        {
            dir = m_camera.ScreenToWorldPoint(Input.mousePosition) - m_currentPoint.transform.position;
            dir.Normalize();
            transform.position = m_currentPoint.transform.position +(Vector3)(dir * m_currentPoint.m_radius);
        }

        //Debug.LogFormat($"Dir = {dir}");

        if(dir.magnitude > 0.2f)
        {
            transform.up = dir;
        }

		Debug.DrawRay(transform.position, Vector3.up * m_suckDistance, Color.blue);
        Debug.DrawRay(transform.position, m_rigidBody.velocity, Color.cyan);
    }

    public bool GetIsSucking()
    {
        return (m_forceReleaseSuck == false && m_moveRoutine == null && GetIsSuckDown() && m_grabTrigger.GetSuckedObject() == null);
    }

    public Vector2 GetOffset(Vector2 screenPos)
    {
        return m_camera.ScreenToWorldPoint(screenPos) - transform.position;
    }

    void FixedUpdate()
    {
        if(m_alive == false || GameManager.Instance.GetHasGameStarted() == false)
        {
            return;
        }

        // float x = Input.GetAxis("Horizontal");
        // float y = Input.GetAxis("Vertical");

        // Vector2 dir = new Vector2(x, y);
        Vector2 dir = GetOffset(Input.mousePosition);
        dir.Normalize();

        //Debug.LogFormat($"Dir = {dir}");

        if(dir.magnitude > 0.2f)
        {
            transform.up = dir;
        }

        Rigidbody2D rigidBody = m_grabTrigger.GetSuckedObject();

        m_animator.SetBool("HasGrabbedObject", (rigidBody != null));

        if(rigidBody != null)
        {
            BoxCollider2D col = rigidBody.GetComponent<BoxCollider2D>();
            float distance = (col.size.x * col.transform.localScale.x) + m_heldMoveTowardsOffset;
            //rigidBody.transform.position = Vector3.MoveTowards(rigidBody.transform.position, transform.position + ((Vector3)dir * distance), m_heldMoveTowardsSpeed * Time.deltaTime);
            rigidBody.transform.position = transform.position + ((Vector3)dir * distance);
            rigidBody.transform.transform.rotation = transform.rotation;
        }

        if(GetIsBlowPressed() == true)
        {
            m_rigidBody.AddForce(-dir * m_thrustPassiveForce * Time.deltaTime);
        }

        if(GetIsBlowDown() == true)
        {
            if(rigidBody != null)
            {
                ShootObject();
                return;
            }
            else
            {
                RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, dir, m_thrustDistance, m_suckLayer);

				foreach(var h in hits)
				{
					if(h.collider.CompareTag("Wall") == true)
					{
                        ThrustableWall thrustableWall = h.collider.GetComponent<ThrustableWall>();

                        if(thrustableWall != null)
                        {
                            UnlockPlayer();
                            float strength = m_thrustFalloffCurve.Evaluate(h.distance / m_thrustDistance);
                            m_rigidBody.AddForce(-dir * strength, ForceMode2D.Impulse);
                            m_currentPoint = null;
                        }
					}
				}

                return;
            }
        }

        bool isSucking = GetIsSucking();
        m_animator.SetBool("IsSucking", isSucking);

        bool shouldPlayParticles = (isSucking == true || rigidBody != null);
        if(m_dustParticles.isPlaying != shouldPlayParticles)
        {
            if(shouldPlayParticles == true)
            {
                m_dustParticles.Play();
            }
            else
            {
                m_dustParticles.Stop();
            }
        }

        if(isSucking == true && m_wasSucking == false)
        {
            m_suckStart.Play();
        }

        float targetPass = (isSucking) ? 22000 : 0;
        if(rigidBody != null)
        {
            targetPass = 500;
        }

        m_suckLoopPass.cutoffFrequency = Mathf.MoveTowards(m_suckLoopPass.cutoffFrequency, targetPass, m_passSpeed * Time.deltaTime);

        m_wasSucking = isSucking;

        if(isSucking == true)
        {
            List<Pickupable> suckingObjects = m_suckTrigger.GetObjectsList();

            m_rigidBody.AddForce(dir * m_suckPassiveForce * Time.deltaTime);

            foreach(var v in suckingObjects)
            {
                if(v == null)
                {
                    continue;
                }

                Vector3 offset = v.transform.position - transform.position;
                Vector3 direction = offset.normalized;
				float distance = offset.magnitude;

			
				RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, direction, distance, m_suckLayer);
				bool valid = true;

				foreach(var h in hits)
				{
					if(h.collider.CompareTag("Wall") == true)
					{
						valid = false;
						break;
					}
				}
				
				if(valid == true)
				{
					float t = distance/m_suckDistance;
					float evaluatedT = m_forceFalloffCurve.Evaluate(t);
					//Debug.LogFormat($"t is: {t}, distance: {distance}, m_suckDistance: {m_suckDistance}, curved: {evaluatedT}");
                    v.SuckUpdate();

                    if(v.GetShouldMove())
                    {
                        Rigidbody2D rigidbody = v.GetRigidbody();
					    rigidbody.velocity = Vector2.MoveTowards(rigidbody.velocity, -direction * m_targetSuckForce, evaluatedT * m_suckForce * Time.deltaTime);
                    }
                }
            }            
        }

        m_lastStayingObjects = new List<Pickupable>(m_suckTrigger.GetObjectsList());
        m_suckTrigger.ClearList();
    }

    public void ShootObject()
    {
        Rigidbody2D body = m_grabTrigger.GetSuckedObject();

        if(body == null)
        {
            return;
        }

        body.isKinematic = false;
        BoxCollider2D box = body.GetComponent<BoxCollider2D>();
        body.position = transform.up * (box.size.x * box.transform.localScale.x + m_heldMoveTowardsOffset);
        body.AddForce(transform.up * m_shootForce, ForceMode2D.Impulse);
        body.transform.SetParent(null);
        m_grabTrigger.Clear();
        m_forceReleaseSuck = true;
        UnlockPlayer();

        m_shootSound.Play();
        m_animator.Play("PlayerShoot");
    }

    public void MoveToPoint(Vector3 endPos, MovePoint movePoint)
    {
        if(m_moveRoutine != null || movePoint == m_currentPoint)
        {
            return;
        }

		m_currentPoint = movePoint;
        m_moveRoutine = StartCoroutine(MoveAnim(endPos));
    }

    public void AddForce(Vector2 force, float t)
    {
        m_rigidBody.velocity = Vector2.MoveTowards(m_rigidBody.velocity, force, t);
    }

    public void PlaySuckInAnim()
    {
        m_animator.Play("PlayerSuckIn");
    }

    public bool GetIsBlowDown()
    {
        return (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.Space));
    }

    public bool GetIsBlowPressed()
    {
        return (Input.GetMouseButton(1) || Input.GetKey(KeyCode.Space));
    }

    public bool GetIsSuckDown()
    {
        return Input.GetMouseButton(0);
    }

    public bool GetForceReleaseSuck()
    {
        return m_forceReleaseSuck;
    }

    public void SetForceReleaseSuck()
    {
        m_forceReleaseSuck = true;
    }

    IEnumerator MoveAnim(Vector3 endPos)
    {
        Vector3 startPos = transform.position;
        float dt = 0;
        float moveTime = Vector3.Distance(startPos, endPos) * m_moveTimeScale;

        while(dt <= moveTime)
        {
            dt += Time.deltaTime;
			float t = dt / moveTime;
			Vector3 currentPos = Vector3.Lerp(startPos, endPos, m_moveCurve.Evaluate(dt / moveTime));
            transform.position = currentPos;

			//Debug.LogFormat($"dt is: {dt}, t is: {t}, startPos is: {startPos}, endPos is: {point}, currentPos: {currentPos}, rigidBod

            yield return false;
        }
		
		CameraController.Instance.ShakeGentle();
        m_moveRoutine = null;
        LockPlayer();
    }

	public MovePoint GetCurrentPoint()
	{
		return m_currentPoint;
	}

    public List<Pickupable> GetLastList()
    {
        return m_lastStayingObjects;
    }

    public void LockPlayer()
    {
        m_rigidBody.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public void UnlockPlayer()
    {
        m_rigidBody.constraints = RigidbodyConstraints2D.None;
    }

    public Rigidbody2D GetSuckedObject()
    {
        return m_grabTrigger.GetSuckedObject();
    }

    public void SetCurrentPoint(MovePoint movePoint)
    {
        m_currentPoint = movePoint;
    }

    public void Kill()
    {
        if(m_alive == false)
        {
            return;
        }

        CameraController.Instance.ShakeStrong();
        StartCoroutine(DeathFlash());
        m_alive = false;

        if(m_moveRoutine != null)
        {
            StopCoroutine(m_moveRoutine);
        }
    }

    public bool GetIsAlive()
    {
        return m_alive;
    }

    IEnumerator DeathFlash()
    {
        float dt = 0;
        Color startColour = Color.red;
        Color endColor = Camera.main.backgroundColor;

        const float FLASH_TIME = 0.6f;
        while(dt < FLASH_TIME)
        {
            dt += Time.deltaTime;
            Camera.main.backgroundColor = Color.Lerp(startColour, endColor, Mathf.SmoothStep(0, 1, dt / FLASH_TIME));

            yield return false;
        }
    }
}

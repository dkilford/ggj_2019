﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class RotateAnimation : MonoBehaviour
{
    public float m_rotateSpeed = 360.0f;
    Rigidbody2D m_rigidBody = null;

    float m_rotation = 0;

    void Start()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
        if(m_rigidBody == null)
        {
            m_rigidBody = gameObject.AddComponent<Rigidbody2D>();
        }

        m_rigidBody.isKinematic = true;
        m_rigidBody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
    }


    void FixedUpdate()
    {
        m_rotation += m_rotateSpeed * Time.deltaTime;
        m_rigidBody.MoveRotation(m_rotation);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class GameManager : MonoSingleton<GameManager>
{
    public TMP_Text m_percentageText = null;
    public WinCondition m_winCondition;
    public float m_timeLimit = 60;
    public float m_completionPercentage = 100.0f;
    public bool m_playIntro = true;

    List<Binable> m_binableObjects = null;
    int m_startCount = 0;
    int m_binnedCount = 0;
    float m_dt = 0;
    bool m_gameStarted = false;

    void Start()
    {
        m_binableObjects = new List<Binable>(FindObjectsOfType<Binable>());

        foreach(var v in m_binableObjects)
        {
            m_startCount += v.m_binableValue;
        }

        if(m_playIntro == true)
        {
            m_gameStarted = false;
            UIManager.Instance.PlayIntroAnimation(m_winCondition, m_timeLimit, m_completionPercentage);
        }
    }


    void LateUpdate()
    {
        if(m_percentageText == null)
        {
            return;
        }

        if(m_gameStarted == true)
        {
            if(PlayerController.Instance.GetIsAlive() == false)
            {
                m_percentageText.text = "You died!";
                return;
            }
            else
            {
                if(m_winCondition == WinCondition.COMPLETION)
                {
                    for(int i=0; i<m_binableObjects.Count; i++)
                    {
                        Binable binable = m_binableObjects[i];

                        if(binable == null)
                        {
                            m_binnedCount++;
                            m_binableObjects.RemoveAt(i);
                            i--;
                        }
                    }

                    m_percentageText.text = string.Format("{0}/{1} value binned", m_binnedCount, m_startCount);

                    if(m_binnedCount / ((float) m_startCount) >= m_completionPercentage / 100)
                    {
                        UIManager.Instance.PlayLevelComplete();
                    }
                }
                else if(m_winCondition == WinCondition.TIME)
                {
                    m_dt += Time.deltaTime;

                    if(m_binnedCount >= m_startCount)
                    {
                        UIManager.Instance.PlayLevelComplete();
                    }
                    else
                    {
                        if(m_dt <= m_timeLimit)
                        {
                            m_percentageText.text = string.Format("Time left: {0}", (m_timeLimit - m_dt).ToString("0"));
                        }
                        else
                        {
                            m_percentageText.text = string.Format("Out of time, press escape to retry!");
                            PlayerController.Instance.Kill();
                        }
                    }
                }
            }
        }
        else if(m_winCondition == WinCondition.TIME)
        {
            m_percentageText.text = string.Format("Time left: {0}", (m_timeLimit).ToString("0"));
        }
        else
        {
            m_percentageText.text = "";
        }
    }

    public void OnBinned(Binable binable)
    {
        if(binable != null)
        {
            m_binnedCount += binable.m_binableValue;
            m_binableObjects.Remove(binable);
        }
    }

    public void OnIntroComplete()
    {
        m_gameStarted = true;
    }

    public bool GetHasGameStarted()
    {
        return m_gameStarted;
    }
}

public enum WinCondition
{
    NONE,
    TIME,
    COMPLETION
}

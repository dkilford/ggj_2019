﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class BinTrigger : MonoBehaviour
{
    public ParticleSystem m_particles = null;

    Animator m_animator = null;
    List<BinableTime> m_insideObjects = new List<BinableTime>();

    bool m_playerHadObject = false;
    
    const float STAY_TIME = 0.2f;

    void Start()
    {
        m_animator = GetComponent<Animator>();
    }

    void Update()
    {
        bool playerHasObject = (PlayerController.Instance.GetSuckedObject() != null);

        if(playerHasObject != m_playerHadObject)
        {
            m_playerHadObject = playerHasObject;
            if(m_playerHadObject == true)
            {
                m_animator.Play("BinFlapOpen");
            }
            else
            {
                m_animator.Play("BinFlapClose");
            }
        }
    }


    void FixedUpdate()
    {
        for(int i=0; i<m_insideObjects.Count; i++)
        {
            BinableTime binableTime = m_insideObjects[i];

            if(binableTime.m_binable == null)
            {
                m_insideObjects.RemoveAt(i);
                i--;
            }
            else
            {
                binableTime.m_dt += Time.deltaTime;
                //Debug.LogFormat($"Bin dt is: {binableTime.m_dt}");

                if(binableTime.m_dt >= STAY_TIME)
                {
                    if(m_particles != null && binableTime.m_binable.m_destroyOnCollect == false)
                    {
                        m_particles.Play();
                    }

                    m_insideObjects.RemoveAt(i);
                    i--;

                    binableTime.m_binable.Bin();
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Binable binable = col.GetComponent<Binable>();
        if(binable != null)
        {
            m_insideObjects.Add(new BinableTime(binable));
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        Binable binable = col.GetComponent<Binable>();
        if(binable != null)
        {
            for(int i=0; i<m_insideObjects.Count; i++)
            {
                if(m_insideObjects[i].m_binable == binable)
                {
                    m_insideObjects.RemoveAt(i);
                    i--;
                }
            }
        }
    }
}

public class BinableTime
{
    public Binable m_binable = null;
    public float m_dt = 0;

    public BinableTime(Binable binable)
    {
        m_binable = binable;
    }
}

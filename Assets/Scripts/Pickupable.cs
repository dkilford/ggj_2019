﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[SelectionBase]
public class Pickupable : MonoBehaviour
{
    public float m_wiggleWaitTime = 0.25f;
    public float m_wiggleWaitMaxTime = 0.35f;
    public float m_wriggleFalloffSpeed = 2.0f;

    const float INSTANT_MOVE_RANGE = 2.0f;

    PlayerController m_pController = null;
    Animator m_animator = null;
    Rigidbody2D m_rigidBody = null;
    bool m_idle = true;
    float m_wriggleDt = 0;
    bool m_wasSucked = false;

    void Start()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        m_pController = PlayerController.Instance;
    }

    void FixedUpdate()
    {
        if(m_wasSucked == true)
        {
            PickupUpdate();
        }
        else
        {
            NotPickingUpUpdate();
        }

        m_wasSucked = false;
    }

    public void SuckUpdate()
    {
        m_wasSucked = true;
    }

    public void PickupUpdate()
    {
        if(m_idle == true)
        {
            m_animator.Play("PickupAnim");
            m_idle = false;
        }

        m_wriggleDt += Time.deltaTime;
        m_wriggleDt = Mathf.Clamp(m_wriggleDt, 0, m_wiggleWaitMaxTime);
    }

    public void NotPickingUpUpdate()
    {
        if(m_idle == false)
        {
            m_animator.Play("PickupAnimIdle");
            m_idle = true;
        }

        m_wriggleDt -= Time.deltaTime * m_wriggleFalloffSpeed;
        m_wriggleDt = Mathf.Clamp(m_wriggleDt, 0, m_wiggleWaitMaxTime);
    }

    public void StopPickingUp()
    {
        if(m_idle == false)
        {
            m_animator.Play("PickupAnimIdle");
            m_idle = true;
        }

        m_wriggleDt = 0;
    }

    public Rigidbody2D GetRigidbody()
    {
        return m_rigidBody;
    }

    public Vector2 GetPosition()
    {
        return m_rigidBody.position;
    }

    public bool GetShouldMove()
    {
        float distance = Vector3.Distance(transform.position, m_pController.transform.position);
        return (m_wriggleDt >= m_wiggleWaitTime || distance < INSTANT_MOVE_RANGE);
    }
}

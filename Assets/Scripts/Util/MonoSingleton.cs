using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour  where T : MonoSingleton<T>
{
	public static bool exists
	{
		get
		{
			return instance !=null;
		}
	}
	
	private static T instance = null;
	public static T Instance
	{
		get
		{
			//if it has no prior instance, find one
			if(instance==null)
			{
				instance = FindObjectOfType<T>();
				
				//didn't find instance
				if(instance == null)
				{
					instance = new GameObject(typeof(T).ToString()).AddComponent<T>();
					
					if(instance == null)
					{
						Debug.LogError("Error creating: "+typeof(T).ToString()); 
					}
				}				
			}
			
			return instance;
		}
	}
	
	protected virtual void Awake()
	{
		if(instance == null)
		{
			instance = this as T;
		}
	}
	
	private void OnDestroy()
	{
		instance = null;
	}
	
	private void OnApplicationQuit()
	{
		instance = null;
	}
}

using UnityEngine;
using System.Collections;

public class SmoothFollow2D : MonoBehaviour
{
	public Transform m_target;
	public bool m_followX = true;
	public bool m_followY = true;
	public float m_smoothTime = 0.3f;
	public float m_lookAddition = 0.3f;

	private Vector2 m_velocity;
	Vector3 m_startPos;
	
	void Start()
	{
		m_startPos = transform.position;
	}
	
	void LateUpdate() 
	{
		if (m_target == null)
		{
			return;
		}

		Vector3 pos = m_target.transform.position;
		Vector3 temp = transform.position;

		if(m_followX)
		{
			temp.x = Mathf.SmoothDamp(transform.position.x, pos.x, ref m_velocity.x, m_smoothTime);
		}
		
		if(m_followY)
		{
			temp.y = Mathf.SmoothDamp(transform.position.y, pos.y, ref m_velocity.y, m_smoothTime);
		}
		
		transform.position = temp;
	}
}
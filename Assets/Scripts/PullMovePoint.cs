﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class PullMovePoint : MovePoint
{
    public float m_maxVelocitySpeed = 0.2f;
    public float m_moveForce = 10.0f;
    public float m_snapDistance = 0.5f;
    public bool m_holdToKeepMoving = false;

    public override void SuckUpdateCompletion()
    {
        Vector3 dir = transform.position - m_pController.transform.position;
        float distance = dir.magnitude;

        if(m_orbit == true)
        {
            distance = Vector3.Distance(m_pController.transform.position, GetOrbitPoint());
        }

        if((m_holdToKeepMoving == false || m_pController.GetIsSucking() == true) && distance < m_snapDistance)
        {
            LockPlayerToPoint();
        }
        else
        {
            dir.Normalize();
            m_pController.UnlockPlayer();

            //Debug.LogFormat($"stuff is: {dir * m_moveForce} , {m_maxVelocitySpeed * Time.deltaTime}");
            m_pController.SetCurrentPoint(this);
            m_pController.AddForce(dir * m_moveForce, m_maxVelocitySpeed * Time.fixedDeltaTime);
        }
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void SuckFailUpdate()
    {
        base.SuckFailUpdate();

        if(m_pController.GetCurrentPoint() == this)
        {
            if(m_orbit == false)
            {
                Vector3 dir = transform.position - m_pController.transform.position;
                float dot = Vector3.Dot(dir.normalized, m_pController.transform.up);

                if((m_holdToKeepMoving == false || (m_pController.GetIsSucking() == true && dot < m_dotAngle)) && dir.magnitude < m_snapDistance)
                {
                    LockPlayerToPoint();
                }
            }
            else
            {
                float distance = Vector3.Distance(GetOrbitPoint(), m_pController.transform.position);
                Vector3 dir = transform.position - m_pController.transform.position;
                float dot = Vector3.Dot(dir.normalized, m_pController.transform.up);

                if((m_holdToKeepMoving == false || (m_pController.GetIsSucking() == true && dot < m_dotAngle)) && distance< m_snapDistance)
                {
                    LockPlayerToPoint();
                }
            }
        }
        else
        {
            m_playerStuck = false;
        } 
    }

    public void LockPlayerToPoint()
    {
        float distance = Vector3.Distance(transform.position, GetOrbitPoint());
        Debug.LogFormat($"Snapping player, distance is: {distance}");

        if(m_orbit == true)
        {
            m_pController.transform.position = GetOrbitPoint();
        }
        else
        {
            m_pController.transform.position = transform.position;
        }
        m_pController.LockPlayer();
        m_pController.SetCurrentPoint(this);

        if(m_playerStuck == false)
        {
            m_pController.SetForceReleaseSuck();
            m_playerStuck = true;
        }
    }
}

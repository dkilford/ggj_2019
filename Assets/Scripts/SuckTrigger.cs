﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class SuckTrigger : MonoBehaviour
{
    public List<Pickupable> m_overlappingObjects = new List<Pickupable>();

    void Start()
    {
        
    }


    void Update()
    {
        
    }

    void OnTriggerStay2D(Collider2D col)
    {
        //Debug.LogFormat($"{col.name} is inside trigger");
        Pickupable pickupableObject = col.GetComponent<Pickupable>();

        if(pickupableObject != null)
        {
            m_overlappingObjects.Add(pickupableObject);
        }
    }

    public void ClearList()
    {
        m_overlappingObjects.Clear();
    }

    public List<Pickupable> GetObjectsList()
    {
        return m_overlappingObjects;
    }
}

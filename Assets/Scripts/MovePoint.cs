﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class MovePoint : MonoBehaviour
{
    public Transform m_suckScaleParent = null;
    public float m_suckTime = 2.0f;
    public LayerMask m_moveMask;
    public GameObject m_targetObject = null;
    public float m_dotAngle = -0.95f;
    public bool m_orbit = false;
    public float m_radius = 1.0f;

    protected CircleCollider2D m_collider = null;
    protected PlayerController m_pController = null;
    protected float m_dt = 0;
    protected bool m_inIdleState = true;
    protected Animator m_animator = null;
    protected bool m_playerStuck = false;
    const int HITS_MAX = 30;

    void Start()
    {
        m_pController = PlayerController.Instance;
        m_collider = GetComponent<CircleCollider2D>();
        m_animator = GetComponent<Animator>();
    }

    protected virtual void FixedUpdate()
    {
        bool valid = false;

        if(this is PullMovePoint == false || m_orbit == false)
        {
            m_playerStuck = (m_pController.GetCurrentPoint() == this);
        }

        //Debug.LogFormat($"Player stuck is: {m_playerStuck}");

        //if(m_pController.GetIsSucking() == true && m_pController.GetCurrentPoint() != this && m_pController.GetLastList().Count == 0)
        if(m_pController.GetIsSucking() == true && (m_pController.GetCurrentPoint() != this || this is PullMovePoint) && m_pController.GetSuckedObject() == null)
        {
            Vector2 dir = m_pController.transform.position - transform.position;
            float distance = dir.magnitude;
            float dot = Vector3.Dot(dir.normalized, m_pController.transform.up);
            //Debug.LogFormat($"Dot is: {dot}");

            if(distance <= m_pController.m_suckDistance && dot < m_dotAngle)
            {
                dir.Normalize();

                RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, dir, distance, m_moveMask);
                valid = true;

                foreach(var v in hits)
                {
                    if(v.collider != null)
                    {
                        if(v.collider.gameObject == m_targetObject)
                        {
                            break;
                        }

                        //Debug.LogFormat($"Hit is: {v.collider.name}");
                        if(v.collider.CompareTag("Suckable") == true || v.collider.CompareTag("Wall"))
                        {
                            Debug.DrawRay(transform.position, dir * v.distance, Color.red);
                            //Debug.LogFormat($"Failing on object: {v.collider.name}");
                            valid = false;
                            break;
                        }
                    }
                }
            }
        }

        if(valid == true)
        {
            Debug.DrawRay(transform.position, Vector3.up, Color.green);
            SuckUpdate();
        }
        else
        {
            SuckFailUpdate();
        }

        //Debug.LogFormat($"t is: {m_dt / m_suckTime}, valid is: {valid}");

        m_suckScaleParent.transform.localScale = new Vector3(m_dt / m_suckTime, 1, 1);
    }

    public virtual void SuckUpdate()
    {
        m_dt += Time.deltaTime;

        if(m_dt >= m_suckTime)
        {
            //Debug.LogFormat("Moving to point!");
            // move player to point
            SuckUpdateCompletion();
        }

        if(m_inIdleState == true)
        {
            m_inIdleState = false;
            m_animator.Play("MovePointSuck");
        }

        m_dt = Mathf.Clamp(m_dt, 0, m_suckTime);
    }

    public virtual void SuckUpdateCompletion()
    {
        m_pController.MoveToPoint(transform.position, this);
        m_playerStuck = true;
    }

    public virtual void SuckFailUpdate()
    {
        Debug.DrawRay(transform.position, Vector3.up, Color.red);
        m_dt -= Time.deltaTime;
        m_dt = Mathf.Clamp(m_dt, 0, m_suckTime);
        //m_playerStuck = false;

        if(m_inIdleState == false)
        {
            m_inIdleState = true;
            m_animator.Play("MovePointIdle");
        }
    }

    public Vector3 GetOrbitPoint()
    {
        Vector3 dir = m_pController.transform.position - transform.position;
        dir.z = 0;
        dir.Normalize();

        Vector3 pos = transform.position + dir * m_radius;
        return pos;
    }

    public bool GetIsPlayerStuck()
    {
        return m_playerStuck;
    }
}
